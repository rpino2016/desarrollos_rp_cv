<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<div id="CheckProperties">
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<div class="checkbox">
				<label><input type="checkbox" id="CheckRequired">Requerido</label>
			</div>
		</div>
	</div>
	<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
		<div class="checkbox">
			<label><input type="checkbox" id="CheckToggle">Checkeado</label>
		</div>
	</div>
	</div>
	<div class="form-group">
		<label for="Etiqueta" class="col-sm-2 control-label">Etiqueta</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" id="CheckLabel">
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<button type="submit" class="btn btn-default" id="SaveCheckProperties">listo</button>
		</div>
	</div>
</div>