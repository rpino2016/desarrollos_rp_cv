<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<div id="TextProperties">
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<div class="checkbox">
				<label><input type="checkbox" id="TexRequired">Requerido</label>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label for="Etiqueta" class="col-sm-2 control-label">Etiqueta</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" id="TexLabel">
		</div>
	</div>
	<div class="form-group">
		<label for="type" class="col-sm-2 control-label">Tipo</label>
		<div class="col-sm-10">
			<select class="form-control" id="TexType">
				<option value="text">Texto</option>
				<option value="password">Contraseņa</option>
				<option value="email">Email</option>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label for="Length" class="col-sm-2 control-label">Longitud</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" id="TextLength">
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<button type="submit" class="btn btn-default" id="SaveTextProperties">listo</button>
		</div>
	</div>
</div>