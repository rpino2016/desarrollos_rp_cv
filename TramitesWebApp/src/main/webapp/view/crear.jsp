<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>crear formulario</title>
		<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="../css/webforms.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script><!-- no olvidar traer la libreria localmente -->
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<script src="../js/webforms.js"></script>
		
		<style type="text/css">
			.fixed {
			  /* height: 480px; */
			  height: calc(100vh - 200px);
			  overflow-y: auto; 
			}
		</style>
	</head>
	<body>
	<div class="container">
		<div class="row">
			<div class="col-md-4">
					<div class="form-group">
						<label for="">Nombre Formulario</label> <input type="text" class="form-control" id="">
					</div>
			</div>
		</div>
		<div class="row">	
				<div class="col-md-6">
					<div class="panel panel-primary">
						<div class="panel-heading">Vista M�vil</div>
							<div class="panel-body fixed">
								<form id="Preview">									
								</form>
							</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="panel panel-primary">
						<div class="panel-heading">Opciones</div>
							<div class="panel-body fixed" >
								<ul class="nav nav-tabs">
									<li class="active"><a data-toggle="tab" href="#ComponentsTab">Componentes</a></li>
									<li><a data-toggle="tab" href="#PropertiesTab">Propiedades</a></li>
								</ul>
								<div class="tab-content">
									<div id="ComponentsTab" class="tab-pane active">
										<ul class="list-group">
											<li class="list-group-item"><a class="icon-text-input" href="#" id="TextField">Campo de Texto</a></li>	
											<li class="list-group-item"><a class="icon-checkbox" href="#" id="CheckBox">CheckBox</a></li>															
											<li class="list-group-item"><a class="icon-checkbox-group" href="#" id="CheckBoxGroup">Campo de Grupo de Seleccion</a></li>
											<li class="list-group-item"><a class="icon-calendar" href="#" id="DateField">Campo de Fecha</a></li>
											<li class="list-group-item"><a class="icon-text-area" href="#" id="DescriptionField">Campo de Descripcion</a></li>
											<li class="list-group-item"><a class="icon-file-input" href="#" id="FileBrowser">Campo de Archivo</a></li>
											<li class="list-group-item"><a class="icon-radio-group" href="#" id="RadioField">Campo de Unica Seleccion</a></li>
											<li class="list-group-item"><a class="icon-select" href="#" id="ListFiel">Campo de Lista</a></li>
											<li class="list-group-item"><a class="icon-button-input" href="#" id="ButtonField">Campo de Bot�n</a></li>
										</ul>
									</div>
									<div id="PropertiesTab" class="tab-pane">
										<form id="Properties" class="form-horizontal">							
										</form>
									</div>
								</div>	
							</div>
						</div>				
				</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<button id="SaveForm" type="button" class="btn btn-primary">Guardar</button>
				<button id="ShowXML" type="button" class="btn btn-primary" data-toggle="modal" data-target="#XMLModal">ver xml</button>				
			</div>
		</div>
	</div>
	
	<div id="XMLModal" class="modal fade" role="dialog" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
      			<div class="modal-header">
      			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      				<h4 class="modal-title">XML Formulario</h4>
      			</div>
      			<div class="modal-body">
			    	<pre lang="xml"></pre>
			    </div>
			    <div class="modal-footer">
			    	<p>modal footer</p>
			    </div>
      		</div>
		</div>
	</div>
	
</body>
</html>