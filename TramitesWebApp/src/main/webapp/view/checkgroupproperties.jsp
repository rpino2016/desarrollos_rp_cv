<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<div id="CheckGroupProperties">
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<div class="checkbox">
				<label><input type="checkbox" id="CheckGroupRequired">Requerido</label>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label for="Etiqueta" class="col-sm-2 control-label">Etiqueta</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" id="CheckGroupLabel">
		</div>
	</div>
	<div id="ListOptions" class="form-group">
		<label for="Etiqueta" class="col-sm-2 control-label">Opciones</label>
		<div class="col-sm-10">
			<ul class="list-group">
				<li class="list-group-item">
					<input type="checkbox"/>
					<input type="text" placeholder="Etiqueta" value="Opcion 1"/>
					<input type="text" placeholder="Valor" value="Opcion 1"/>
				</li>	
				<li class="list-group-item">
					<input type="checkbox"/>
					<input type="text" placeholder="Etiqueta" value="Opcion 2"/>
					<input type="text" placeholder="Valor" value="Opcion 2"/>
				</li>																				
			</ul>	
			<button id="AddCheckGroupOption" type="button" class="btn btn-default col-sm-offset-8" aria-label="Left Align">agregar opcion
				<span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
			</button>		
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<button type="submit" class="btn btn-default" id="SaveCheckGroupProperties">listo</button>
		</div>
	</div>
</div>