$(document).on('ready',function(){
	
	var XMLForm = $.parseXML('<?xml version="1.0" encoding="UTF-8"?>\n<form>\n<fields>\n</fields>\n</form>');

	//$("#Preview").load('preview.jsp');
	//----------------TEXTFIELD------------------------------------
	//crear campos textfield en html y xml DOM
	var TextId = 1;
	$( '#TextField' ).click(function() {	
		TextFieldNode = XMLForm.createElement('field');
		TextFieldNode.setAttribute("id","text"+TextId);
		TextFieldNode.setAttribute("label","Etiqueta");
		TextFieldNode.setAttribute("type","text");
		TextFieldNode.setAttribute("required","false");		
		//TextFieldNode.appendChild(document.createTextNode('bar'));// agrega valor a un elemento
		XMLForm.getElementsByTagName("fields")[0].appendChild(TextFieldNode);	
		$( "#Preview" ).append('<div id="'+'text'+TextId+'" class="form-group"><label for="TextFiel">Campo de Texto:</label><a  href="#" class="edittext">   editar</a> | <a class="removetext" href="#"> eliminar</a><input type="text" class="form-control"></div>');
		TextId++;	
	});	
	//Editar propiedades del textField
	$( '#Preview' ).on('click','.edittext',function() {
		FormTextElement = XMLForm.getElementById($(this).parent().attr('id'));
		$('a[href="#PropertiesTab"]').tab('show');
			$("#Properties").load('textproperties.jsp',function(){
				if(FormTextElement.getAttribute('required')=='true'){
					$("#TexRequired").prop('checked', true);
				}
				$('#TexLabel').val(FormTextElement.getAttribute('label'));
				$('#TexType').val(FormTextElement.getAttribute('type'));
				$('#TextLength').val(FormTextElement.getAttribute('length'));
			});			
		EditText = $(this).parent();
		});
	//Guardar propiedades de textfield
	$('#Properties').on('click','#SaveTextProperties',function(){
		FormTextElement = XMLForm.getElementById(EditText.attr('id'));
		
		if($("#TexRequired").is(':checked')) { 
			FormTextElement.setAttribute("required","true");
			required = '*';
        } else {  
        	FormTextElement.setAttribute("required","false");
        	required = '';
        }
		FormTextElement.setAttribute("label",$('#TexLabel').val());
		FormTextElement.setAttribute("type",$('#TexType').val());
		FormTextElement.setAttribute("length",$('#TextLength').val());
		alert('Propiedades Guardadas');		
		EditText.find('label').text(required+$('#TexLabel').val());
		$('#TextProperties').remove();
	});
	//eliminar textfield
	$( '#Preview' ).on('click','.removetext',function() {		
		FormTextElement = XMLForm.getElementById($(this).parent().attr('id'));
		alert(FormTextElement.getAttribute('id'));
		FormTextElement.parentNode.removeChild(FormTextElement);
		$(this).parent().remove();	
		});
	//-------------------------------------------------------------------------------
	
	//---------------------------CHECKBOX--------------------------------------------
	//crear campo checkbox en html y xml DOM
	var CheckId = 1;
	$( '#CheckBox' ).click(function() {
		CheckBoxNode = XMLForm.createElement('field');
		CheckBoxNode.setAttribute("id","check"+CheckId);
		CheckBoxNode.setAttribute("label","Etiqueta");		
		CheckBoxNode.setAttribute("type","checkbox");
		CheckBoxNode.setAttribute("required","false");
		CheckBoxNode.setAttribute("checked","false");
		//CheckBoxNode.appendChild(document.createTextNode('bar'));// agrega valor a un elemento
		XMLForm.getElementsByTagName("fields")[0].appendChild(CheckBoxNode);
		$( "#Preview" ).append('<div id="'+'check'+CheckId+'" class="checkbox"><label><input type="checkbox"><span>opcion</span></label><a href="#" class="editcheck">   editar</a> | <a href="#" class="removecheck"> eliminar</a></div>');
		CheckId++;
	});	
	//Editar propiedades del checkbox
	$( '#Preview' ).on('click','.editcheck',function() {
		FormCheckElement = XMLForm.getElementById($(this).parent().attr('id'));
		$('a[href="#PropertiesTab"]').tab('show');
			$("#Properties").load('checkproperties.jsp',function(){
				if(FormCheckElement.getAttribute('required')=='true'){
					$("#CheckRequired").prop('checked', true);
				}
				if(FormCheckElement.getAttribute('checked')=='true'){
					$("#CheckToggle").prop('checked', true);
				}
				$('#CheckLabel').val(FormCheckElement.getAttribute('label'));
			});			
		EditCheck = $(this).parent();
		});
	//Guardar propiedades de checkbox
	$('#Properties').on('click','#SaveCheckProperties',function(){
		FormCheckElement = XMLForm.getElementById(EditCheck.attr('id'));
		
		if($("#CheckRequired").is(':checked')) { 
			FormCheckElement.setAttribute("required","true");
			required = '*';
        } else {  
        	FormCheckElement.setAttribute("required","false");
        	required = '';
        }
		if($("#CheckToggle").is(':checked')) { 
			FormCheckElement.setAttribute("checked","true");
			EditCheck.find('input').prop('checked', true);
        } else {  
        	FormCheckElement.setAttribute("checked","false");
        	EditCheck.find('input').prop('checked', false);
        }
		FormCheckElement.setAttribute("label",$('#CheckLabel').val());
		alert('Propiedades Guardadas');		
		EditCheck.find('span').text(required+$('#CheckLabel').val());
		
		$('#CheckProperties').remove();
	});
	//eliminar checkbox
	$( '#Preview' ).on('click','.removecheck',function() {		
		FormCheckElement = XMLForm.getElementById($(this).parent().attr('id'));
		alert(FormCheckElement.getAttribute('id'));
		FormCheckElement.parentNode.removeChild(FormCheckElement);
		$(this).parent().remove();	
		});
	//---------------------------------------------------------------------------------
	
	//---------------------------CHECKBOX GROUP--------------------------------------------
	//crear campos checkbox group en html y xml DOM
	var CheckGroupId = 1;
	$( '#CheckBoxGroup' ).click(function() {
		CheckGroupdNode = XMLForm.createElement('field');
		CheckGroupdNode.setAttribute("id","chekgroup"+CheckGroupId);
		CheckGroupdNode.setAttribute("label","Etiqueta");
		CheckGroupdNode.setAttribute("type","chekgroup");
		CheckGroupdNode.setAttribute("required","false");			
		XMLForm.getElementsByTagName("fields")[0].appendChild(CheckGroupdNode);
		for (i=1; i<=2 ; i++)
			createCheckGroupOption("chekgroup"+CheckGroupId,"option-"+i,"opcion "+i);		
		$( "#Preview" ).append('<div id="chekgroup'+CheckGroupId+'" class="form-group"><label for="CheckGroup">Grupo de Seleccion</label><a href="#" class="editcheckgroup">   editar</a> | <a href="#"> eliminar</a>'
								+'<div class="checkbox"><label><input type="checkbox">Opcion 1</label></div>'
								+'<div class="checkbox"><label><input type="checkbox">Opcion 2</label></div>'
							 +'</div>');
		CheckGroupId++;
		});
	
	//Editar propiedades del checkbox group
	$( '#Preview' ).on('click','.editcheckgroup',function() {
		FormCheckGroupElement = XMLForm.getElementById($(this).parent().attr('id'));
		$('a[href="#PropertiesTab"]').tab('show');
			$("#Properties").load('checkgroupproperties.jsp',function(){
				if(FormCheckGroupElement.getAttribute('required')=='true'){
					$("#CheckGroupRequired").prop('checked', true);
				}
				if(FormCheckGroupElement.getAttribute('checked')=='true'){
					$("#CheckToggle").prop('checked', true);
				}
				$('#CheckGroupLabel').val(FormCheckGroupElement.getAttribute('label'));
			});	
		$("#Properties").on('click','#AddCheckGroupOption',function(){
			$(this).prev().append('<li class="list-group-item">'
									  +'<input type="checkbox" class="fix"/>'
									  +'<input type="text" placeholder="Etiqueta" class="fix" style="margin-left:4px;"/>'
									  +'<input type="text" placeholder="Valor" class="fix" style="margin-left:4px;"/>'
								 +'</li>');
			});
		EditCheckGroup = $(this).parent();
		});
	//Guardar propiedades de checkbox group
	$('#Properties').on('click','#SaveCheckGroupProperties',function(){
		FormCheckGroupElement = XMLForm.getElementById(EditCheckGroup.attr('id'));
		
		if($("#CheckGroupRequired").is(':checked')) { 
			FormCheckGroupElement.setAttribute("required","true");
			required = '*';
        } else {  
        	FormCheckGroupElement.setAttribute("required","false");
        	required = '';
        }
		if($("#CheckToggle").is(':checked')) { 
			FormCheckGroupElement.setAttribute("checked","true");
			EditCheckGroup.find('input').prop('checked', true);
        } else {  
        	FormCheckGroupElement.setAttribute("checked","false");
        	EditCheckGroup.find('input').prop('checked', false);
        }
		FormCheckGroupElement.setAttribute("label",$('#CheckGroupLabel').val());
		alert('Propiedades Guardadas');		
		EditCheckGroup.find('label[for="CheckGroup"]').text(required+$('#CheckGroupLabel').val());
		
		
		$('#CheckProperties').remove();
	});
	
	function createCheckGroupOption(IdNode, Value, Text){
		CheckGroupOption = XMLForm.createElement('option');
		CheckGroupOption.setAttribute("value",Value);
		var textnode = XMLForm.createTextNode(Text);
		CheckGroupOption.appendChild(textnode);
		XMLForm.getElementById(IdNode).appendChild(CheckGroupOption);
	}
	
	//---------------------------------------------------------------------------------
	
	$( '#DateField' ).click(function() {
		$( "#Preview" ).append('<div class="form-group"><label for="Date">Fecha:</label><a href="#">   editar</a> | <a href="#"> eliminar</a><input label="Date Field" type="date" class="form-control calendar"></div>');
		});
	
	$( '#DescriptionField' ).click(function() {
		$( "#Preview" ).append('<div class="form-group"><label for="comment">Descripcion:</label><a href="#">   editar</a> | <a href="#"> eliminar</a><textarea class="form-control" rows="4" cols="50"/></div>');
		});
	
	$( '#FileBrowser' ).click(function() {
		$( "#Preview" ).append('<div class="form-group"><label for="File">Archivo:</label><a href="#">   editar</a> | <a href="#"> eliminar</a><input type="file" class="form-control-file" id="exampleInputFile"></div>');
		});
	
	$( '#RadioField' ).click(function() {
		$( "#Preview" ).append('<div class="radio"><label><input type="radio" name="optradio">Unica Opcion</label><a href="#">   editar</a> | <a href="#"> eliminar</a></div>');
		});
	
	$( '#ListFiel' ).click(function() {
		$( "#Preview" ).append('<div class="form-group">'
								  +'<label for="Select">Lista:</label><a href="#">   editar</a> | <a href="#"> eliminar</a>'
									  +'<select class="form-control" id="sl">'
									  +'<option>1</option>'
									  +'<option>2</option>'
									  +'<option>3</option>'
									  +'<option>4</option>'
									  +'</select>'
								  +"</div>");
		});	
	
	$( '#ButtonField' ).click(function() {
		$( "#Preview" ).append('<div class="form-group"><label for="Button">Boton:</label><a href="#">   editar</a> | <a href="#"> eliminar</a><button class="btn btn-default form-control">Boton</button></div>');
		});
	
	
	//Boton guardar formulario
	
	$( '#SaveForm' ).click(function() {
		});
	
	$('#XMLModal').on('show.bs.modal', function (event) {
		xmlString = (new XMLSerializer()).serializeToString(XMLForm);
		$(this).find('pre').text(formatXml(xmlString));
	});	
	
	
	
	//Formatear un string xml para visualizar
	function formatXml(xml) {
	    var formatted = '';
	    var reg = /(>)(<)(\/*)/g;
	    xml = xml.replace(reg, '$1\r\n$2$3');
	    var pad = 0;
	    jQuery.each(xml.split('\r\n'), function(index, node) {
	        var indent = 0;
	        if (node.match( /.+<\/\w[^>]*>$/ )) {
	            indent = 0;
	        } else if (node.match( /^<\/\w/ )) {
	            if (pad != 0) {
	                pad -= 1;
	            }
	        } else if (node.match( /^<\w[^>]*[^\/]>.*$/ )) {
	            indent = 1;
	        } else {
	            indent = 0;
	        }
	        var padding = '';
	        for (var i = 0; i < pad; i++) {
	            padding += '  ';
	        }

	        formatted += padding + node + '\r\n';
	        pad += indent;
	    });

	    return formatted;
	}
	
});