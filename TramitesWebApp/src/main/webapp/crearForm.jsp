<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" session="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" media="screen"
	href="../js/form-builder.css">
</head>
<body>
<textarea id="fb-template"></textarea>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
	<script src="../js/form-builder.js"></script>
	<script>
		jQuery(document).ready(function($) {
			'use strict';
			var fbTemplate = document.getElementById('fb-template');
			$(fbTemplate).formBuilder();

			document.addEventListener('saveForm', function(e) {
				
				var formName = prompt('Enter your form name: ','form');
				if(formName!=null){
					$.ajax({
						method : "GET",
						url : "/do",
						data : {
							action: "SaveForm",
							name : formName,
							formXML : fbTemplate.value
						},
						async: false,
				      	dataType: 'html',
				      	success: function(response){
				      		alert('Guardado exitosamente');
				      		window.location.href = "index.jsp";
				      	}
					})
				}
			}, false);
		});
	</script>
</body>
</html>