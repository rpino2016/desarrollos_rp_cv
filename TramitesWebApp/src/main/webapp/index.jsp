<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	<title>webforms</title>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script><!-- no olvidar traer la libreria localmente -->
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<style type="text/css">
		/* Large desktop */
		@media (min-width: 1200px){

		}

		/* Portrait tablet to landscape and desktop */
		@media (min-width: 768px) and (max-width: 979px){ 

		}

		/* Landscape phone to portrait tablet */
		@media (max-width: 767px){

		}

		/* Landscape phones and down */
		@media (max-width: 480px){

		} 

		.panel{
			margin-top: 10px;
		}

	</style>
</head>
<body>
	<div class="container-fluid">
		<div class="row-fluid">
			<div class="col-sm-12">
				<h1 class="text-center">Web Forms</h1>					
			</div>
		</div>
		<div class="row-fluid">
			<div class="col-sm-12">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#home">Home</a></li>
					<li><a data-toggle="tab" href="#parametricas">Parametricas</a></li>
					<li><a data-toggle="tab" href="#tramites">Tr�mites</a></li>
					<li><a data-toggle="tab" href="#od">Origenes de Datos</a></li>
					<li><a data-toggle="tab" href="#notificaciones">Notificaciones</a></li>
					<li><a data-toggle="tab" href="#cs">Costos del Sistema</a></li>
				</ul>
				<div class="tab-content">
					<div id="home" class="tab-pane fade in active">
						<div class="row-fluid"><!-- revisar si es necesario este div -->
							<div class="col-sm-3">
								<div class="panel panel-primary">
									<div class="panel-heading">Tr�mites Disponibles</div>
									<div class="panel-body">
										<ul class="nav nav-pills nav-stacked">
											<li><a href="#">Tr�mite 1</a></li>
											<li><a href="#">Tr�mite 2</a></li>
											<li><a href="#">Tr�mite 3</a></li>
											<li><a href="#">Tr�mite 4</a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-sm-9">
								<div class="panel panel-primary">
									<div class="panel-heading">Estad�sticas</div>
									<div class="panel-body">Estadisticas de tr�mites</div>
								</div>
							</div>
						</div>
					</div>
					<div id="parametricas" class="tab-pane fade">
						<div class="col-sm-12">
								<div class="panel panel-primary">
									<div class="panel-heading">Tr�mites Disponibles</div>
									<div class="panel-body">
										<div class="table-responsive">
											<table class="table">
												<thead>
													<tr>
														<th>#</th>
														<th>Parametrica</th>
														<th>Acci�n</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>1</td>
														<td>Parametrica 1</td>
														<td>
															<a href="#" class="btn btn-default btn-sm">editar</a>
															<a href="#" class="btn btn-default btn-sm">borrar</a>
														</td>
													</tr>
													<tr>
														<td>2</td>
														<td>Parametrica 2</td>
														<td>
															<a href="#" class="btn btn-default btn-sm">editar</a>
															<a href="#" class="btn btn-default btn-sm">borrar</a>
														</td>
													</tr>
												</tbody>
											</table>
											<button type="button" class="btn btn-primary">Nuevo</button>
										</div>
									</div>
								</div>
							</div>
					</div>
					<div id="tramites" class="tab-pane fade">
						<div class="row-fluid"><!-- revisar si es necesario este div -->
							<div class="col-sm-12">
								<div class="panel panel-primary">
									<div class="panel-heading">Tr�mites Disponibles</div>
									<div class="panel-body">
										<div class="table-responsive">
											<table class="table">
												<thead>
													<tr>
														<th>#</th>
														<th>Tr�mites Disponibles</th>
														<th>Acci�n</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>1</td>
														<td>Tramite 1</td>
														<td>
															<a href="#" class="btn btn-default btn-sm">editar</a>
															<a href="#" class="btn btn-default btn-sm">borrar</a>
															<a href="#" class="btn btn-default btn-sm">Publicar</a>
														</td>
													</tr>
													<tr>
														<td>2</td>
														<td>Tramite 2</td>
														<td>
															<a href="#" class="btn btn-default btn-sm">editar</a>
															<a href="#" class="btn btn-default btn-sm">borrar</a>
															<a href="#" class="btn btn-default btn-sm">Publicar</a>
														</td>
													</tr>
												</tbody>
											</table>
											<a href="/view/crear.jsp" class="btn btn-primary">Nuevo</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="od" class="tab-pane fade">
						<h3>Origenes de Datos</h3>
						<p>Some content in menu 2.</p>
					</div>
					<div id="notificaciones" class="tab-pane fade">
						<h3>notificaciones</h3>
						<p>Some content in menu 2.</p>
					</div>
					<div id="cs" class="tab-pane fade">
						<h3>Costos del sistema</h3>
						<p>Some content in menu 2.</p>
					</div>
				</div>	
			</div>
		</div>
	</div>	
</body>	
</html>