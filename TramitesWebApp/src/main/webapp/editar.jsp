<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" media="screen"
	href="../js/form-builder.css">
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/t/dt/dt-1.10.11/datatables.min.css" />
</head>
<body>
	<table id="example" class="display" cellspacing="0" width="100%">
	<thead>
		<tr>
		<th>ID</th>
		<th>Nombre</th>
		</tr>
	</thead>
	</table>
	<div id='inputDivGroup'></div>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
	<script type="text/javascript"
		src="https://cdn.datatables.net/t/dt/dt-1.10.11/datatables.min.js"></script>
	<script src="../js/form-builder.js"></script>
	<script>
		jQuery(document).ready(function($) {
			// 'use strict';
			$.ajax({
				method : "GET",
				url : "/do",
				data : {
					action : "GetAll",
				},
				async : false,
				dataType : 'html',
				success : function(json) {
					var input = jQuery.parseJSON(json);
					var table = $('#example').DataTable({
					    "aaData": input,
					    "rowId": 'data',
					    "aoColumns": [
					                  { "mDataProp": "id" },
					                  { "mDataProp": "nombre" }
					              ]
					  });
					
					$('#example').on( 'click', 'tr', function () {
						var inputDiv = document.getElementById('fb-template-div');
						if(inputDiv!=null){
							$("#fb-template-div").remove();
						}
						inputDiv = $(document.createElement('div')).attr("id", 'fb-template-div');
						inputDiv.after().html('<textarea id="fb-template"></textarea>');
						inputDiv.appendTo("#inputDivGroup");
						
						var fbTemplate = document.getElementById('fb-template');
						var data = table.row( this ).id();
						
					    // Elimina saltos de línea
					    data = data.replace(/\r?\n|\r/g, '' );
					    // Elimina backslashes
					    data = data.replace(/\\/g, '');
					    // var xmlData = jQuery.parseXML(data);
					    
					    fbTemplate.value = data;
						$(fbTemplate).formBuilder();
					} );
				}
			})
		});
	</script>
</body>
</html>