package com.example.formBuilder.servlet;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.formBuilder.facade.PersistenceFacade;
import com.example.formBuilder.model.UserModel;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

@SuppressWarnings("serial")
public class LoginServlet extends HttpServlet {
	private UserService userService = UserServiceFactory.getUserService();
	private PersistenceFacade persistenceFacade;
	private final static Logger LOGGER = Logger.getLogger(LoginServlet.class.getName());
	
	public LoginServlet() {
		this.persistenceFacade = PersistenceFacade.getInstance();
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		User currentUser = userService.getCurrentUser();
		String redirectUrl = "./index.jsp";
		String userAgentHeader = req.getHeader("User-Agent");
		// Comprueba si el acceso se hace desde un dispositivo móvil para cambiar a la vista reducida
		if(userAgentHeader.matches(".*(Mobile|Android|IOS|Phone).*")) {
			redirectUrl = "./indexMBL.jsp";
		  }
		if(currentUser == null ){
			 resp.sendRedirect(userService.createLoginURL(req.getRequestURI()));
		}else{
			RequestDispatcher dispatcher = req.getRequestDispatcher(redirectUrl);
			List<UserModel> result = persistenceFacade.getUserByLogin(currentUser.getEmail());
			UserModel user = new UserModel();
			LOGGER.info(" Login encontrado: " + user.toString());
			if(!result.isEmpty()){
				user = result.get(0); // Work with the first coincidence
			}
			req.getSession().setAttribute("userNickName", currentUser.getNickname());
			// req.getSession().setAttribute("userRoles", appUser.getRol());
			// boolean isSuperUser = currentUser.getEmail().equalsIgnoreCase( propHelper.getDefaultAdminMail());
			boolean isSuperUser = true;
			req.getSession().setAttribute("isSuperUser", isSuperUser );
			req.getSession().setAttribute("logoutURL", userService.createLogoutURL("./"));
			dispatcher.forward(req, resp);
		}
	}
}
       
