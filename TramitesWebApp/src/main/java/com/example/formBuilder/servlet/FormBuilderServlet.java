package com.example.formBuilder.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.exception.ExceptionUtils;

import com.example.formBuilder.exception.FormBuilderException;
import com.example.formBuilder.facade.PersistenceFacade;
import com.example.formBuilder.model.FormModel;
import com.example.formBuilder.model.LegacyModel;
import com.example.formBuilder.model.ParameterModel;
import com.example.formBuilder.model.RequesterModel;
import com.example.formBuilder.util.ResultCodeEnum;
import com.example.formBuilder.vo.FormVO;
import com.example.formBuilder.vo.LegacyConfigurationVO;
import com.example.formBuilder.vo.LegacyVO;
import com.example.formBuilder.vo.ParameterVO;
import com.example.formBuilder.vo.RequesterVO;
import com.google.appengine.api.datastore.Text;
import com.google.gson.Gson;
/***
 * Clase servlet para emular el comportamiento de un controller de la aplicación
 * @author rafaelpino
 *
 */
@SuppressWarnings("serial")
public class FormBuilderServlet extends HttpServlet {
	private static final String SAVE_FORM_ACTION ="SaveForm";
	private static final String UPDATE_FORM_ACTION ="UpdateForm";
	private static final String SAVE_PARAMETER_ACTION ="SaveParameter";
	private static final String SAVE_LEGACY_ACTION ="SaveLegacy";
	private static final String SAVE_REQUESTER_ACTION ="SaveRequester";
	private static final String SAVE_LEGACYXFORM_ACTION ="SaveLegacyXForm";
	private static final String ACTIVATE_REQUESTER_ACTION ="ActivateRequester";
	private static final String DEACTIVATE_REQUESTER_ACTION ="DeactivateRequester";
	private static final String BLOCK_REQUESTER_ACTION ="BlockRequester";
	private static final String GET_ALL_FORM_ACTION ="GetAllForms";
	private static final String GET_ALL_PARAMETERS_ACTION ="GetAllParameters";
	private static final String GET_ALL_LEGACIES_ACTION ="GetAllLegacies";
	private static final String GET_ALL_REQUESTERS_ACTION ="GetAllRequesters";
	private PersistenceFacade persistenceFacade;
	
	public FormBuilderServlet(){
		this.persistenceFacade = PersistenceFacade.getInstance();
	}
	
	/**
	 * Método que procesa la petición Get al servlet y evalúa el parámetro action para realizar
	 * la operación correspondiente.
	 * 
	 * Retorna la respuesta en formato JSONForm
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		resp.setContentType("application/json");
		String action = req.getParameter("action");
		Gson gson = new Gson();
		String responseJson = "";
		try{
			switch (action) {
			case FormBuilderServlet.SAVE_FORM_ACTION:
				FormVO formVo = new FormVO();
				formVo.setName(req.getParameter("name"));
				formVo.setXml(new Text(req.getParameter("formXML")));
				persistenceFacade.getAbstractDAO(PersistenceFacade.FORM_DAO).insert(formVo);
				responseJson = gson.toJson(new FormBuilderException(ResultCodeEnum.OK));
				break;
				
			case FormBuilderServlet.UPDATE_FORM_ACTION:
				formVo = new FormVO();
				formVo.setName(req.getParameter("name"));
				formVo.setXml(new Text(req.getParameter("formXML")));
				String formId = req.getParameter("formId");
				if(formId != null && !formId.isEmpty()){
					persistenceFacade.getAbstractDAO(PersistenceFacade.FORM_DAO).update(formVo, Long.valueOf(formId));;
					responseJson = gson.toJson(new FormBuilderException(ResultCodeEnum.OK));
				}else{
					responseJson = gson.toJson(new FormBuilderException(ResultCodeEnum.NO_ID));
				}
				
				break;

			case FormBuilderServlet.SAVE_PARAMETER_ACTION:
				ParameterVO parameterVo = new ParameterVO();
				parameterVo.setName(req.getParameter("name"));
				parameterVo.setDescription(req.getParameter("description"));
				parameterVo.setValues(new Text(req.getParameter("values")));
				persistenceFacade.getAbstractDAO(PersistenceFacade.PARAM_DAO).insert(parameterVo);
				responseJson = gson.toJson(new FormBuilderException(ResultCodeEnum.OK));
				break;
			
			case FormBuilderServlet.SAVE_REQUESTER_ACTION:
				RequesterVO requester = new RequesterVO();
				requester.setName(req.getParameter("name"));
				requester.setLastName(req.getParameter("lastName"));
				requester.setAge(req.getParameter("age")!= null ? Integer.valueOf(req.getParameter("age")): 0);
				requester.setGender(req.getParameter("gender").charAt(0));
				requester.setIdDocument(req.getParameter("idDocument")!= null ? Long.valueOf(req.getParameter("idDocument")): 0);
				requester.setLogin(req.getParameter("login"));
				persistenceFacade.getAbstractDAO(PersistenceFacade.REQUESTER_DAO).insert(requester);
				responseJson = gson.toJson(new FormBuilderException(ResultCodeEnum.OK));
				break;
				
			case FormBuilderServlet.SAVE_LEGACYXFORM_ACTION:
				String legacyId = req.getParameter("legacyId");
				formId = req.getParameter("formId");
				if(legacyId!= null && !legacyId.isEmpty() && formId!=null && !formId.isEmpty()){
					persistenceFacade.insertLegacyXForm(Long.valueOf(formId), Long.valueOf(legacyId) );
				}
				responseJson = gson.toJson(new FormBuilderException(ResultCodeEnum.OK));
				break;
				
			case FormBuilderServlet.ACTIVATE_REQUESTER_ACTION:
				String requesterId = req.getParameter("requesterId");
				if(requesterId != null && !requesterId.isEmpty()){
					persistenceFacade.activateRequester(Long.valueOf(requesterId) );
					responseJson = gson.toJson(new FormBuilderException(ResultCodeEnum.OK));
				}else{
					responseJson = gson.toJson(new FormBuilderException(ResultCodeEnum.NO_PARAMS));
				}
				break;
				
			case FormBuilderServlet.DEACTIVATE_REQUESTER_ACTION:
				requesterId = req.getParameter("requesterId");
				if(requesterId != null && !requesterId.isEmpty()){
					persistenceFacade.deactivateRequester(Long.valueOf(requesterId) );
					responseJson = gson.toJson(new FormBuilderException(ResultCodeEnum.OK));
				}else{
					responseJson = gson.toJson(new FormBuilderException(ResultCodeEnum.NO_PARAMS));
				}
				break;
				
			case FormBuilderServlet.BLOCK_REQUESTER_ACTION:
				requesterId = req.getParameter("requesterId");
				if(requesterId != null && !requesterId.isEmpty()){
					persistenceFacade.blockRequester(Long.valueOf(requesterId) );
					responseJson = gson.toJson(new FormBuilderException(ResultCodeEnum.OK));
				}else{
					responseJson = gson.toJson(new FormBuilderException(ResultCodeEnum.NO_PARAMS));
				}
				break;

			case FormBuilderServlet.SAVE_LEGACY_ACTION:
				LegacyVO legacyVo = new LegacyVO();
				LegacyConfigurationVO configurationVo = new LegacyConfigurationVO();
				legacyVo.setName(req.getParameter("name"));
				legacyVo.setDescription(req.getParameter("description"));
				configurationVo.setName(req.getParameter("configName"));
				configurationVo.setUser(req.getParameter("configUser"));
				configurationVo.setPassword(req.getParameter("configPassword"));
				configurationVo.setActionName(req.getParameter("configActionName"));
				configurationVo.setResponseCaseNumberMapping(req.getParameter("configCaseMapping"));
				configurationVo.setResponseCodeMapping(req.getParameter("configResponseMapping"));
				configurationVo.setResponseMessageMapping(req.getParameter("configResponseMsgMapping"));
				configurationVo.setTimeout(req.getParameter("configTimeout")!= null ? Integer.valueOf(req.getParameter("configTimeout")): 0);
				configurationVo.setUrl(req.getParameter("configURL"));
				legacyVo.setLegacyConfiguration(configurationVo);
				legacyVo.setLegacyType(req.getParameter("legacyType") != null ? Integer.valueOf(req.getParameter("legacyType")) : 0);
				persistenceFacade.getAbstractDAO(PersistenceFacade.LEGACY_DAO).insert(legacyVo);
				responseJson = gson.toJson(new FormBuilderException(ResultCodeEnum.OK));
				break;
				
			case FormBuilderServlet.GET_ALL_FORM_ACTION:
				List<FormModel> forms = persistenceFacade.getForms();
				responseJson = gson.toJson(forms);
				break;
				
			case FormBuilderServlet.GET_ALL_PARAMETERS_ACTION:
				List<ParameterModel> parameters = persistenceFacade.getParameters();
				responseJson = gson.toJson(parameters);
				break;
				
			case FormBuilderServlet.GET_ALL_LEGACIES_ACTION:
				List<LegacyModel> legacies = persistenceFacade.getLegacies();
				responseJson = gson.toJson(legacies);
				break;
				
			case FormBuilderServlet.GET_ALL_REQUESTERS_ACTION:
				List<RequesterModel> requesters = persistenceFacade.getRequesters();
				responseJson = gson.toJson(requesters);
				break;
				
			default:
				resp.setStatus(200);
				break;
			}
			resp.getWriter().write(responseJson);
		}catch (Exception e) {
			if(e instanceof FormBuilderException){
				responseJson = gson.toJson(e);
				resp.getWriter().write(responseJson);
			}else{
				FormBuilderException exception = new FormBuilderException(ResultCodeEnum.UNEXPECTED);
				Throwable rootCause = ExceptionUtils.getRootCause(e);
				if(rootCause != null){
					exception.setDetail(rootCause.getCause() != null ? rootCause.getCause().getMessage() : rootCause.getMessage());
				}else{
					exception.setDetail(e.getMessage());
				}
				
				responseJson = gson.toJson(exception);
				resp.getWriter().write(responseJson);
			}
		}
	}
	
}
