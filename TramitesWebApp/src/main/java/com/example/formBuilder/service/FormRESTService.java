package com.example.formBuilder.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.example.formBuilder.facade.PersistenceFacade;
import com.example.formBuilder.model.FormModel;

public class FormRESTService extends ServerResource {
	PersistenceFacade persistenceFacade;
	
	public FormRESTService(){
		this.persistenceFacade = PersistenceFacade.getInstance();
	}

	@Get
	public String doGet() throws Exception {
		String param = getQueryValue("formId");
		Long formId = null;
		if(param!=null){
			formId = Long.parseLong(param);
		}
		return prettyPrint(buildFormDataXML(formId));

	}

	private Document buildFormDataXML(Long id) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		// root elements
		Document doc = docBuilder.newDocument();
		List<FormModel> forms = new ArrayList<FormModel>();
		if(id!=null){
		 forms.add(persistenceFacade.getFormById(id) );
		}else{
		 forms = persistenceFacade.getFormIdList();	
		}
		
		Element rootElement = doc.createElement("Formularios");
		for (FormModel e : forms) {
			Element formElm = doc.createElement("Formulario");
			formElm.setAttribute("Id", String.valueOf(e.getId()));
			formElm.setAttribute("Nombre", e.getName());
			String xmlData = e.getData();
			if(xmlData!=null){
				Element xmlDataElm = doc.createElement("XMLData");
				ByteArrayInputStream in = new ByteArrayInputStream(xmlData.getBytes());
				Document dataDoc = docBuilder.parse(in);
				NodeList nodeList = dataDoc.getChildNodes();
				for(int i= 0; i<nodeList.getLength();i++){
					Node node = doc.adoptNode(nodeList.item(i));
					xmlDataElm.appendChild(node);
				}
				formElm.appendChild(xmlDataElm);
			}
			rootElement.appendChild(formElm);
		}
		doc.appendChild(rootElement);
		return doc;
	}

	private String prettyPrint(Document xml) throws Exception {
		Transformer tf = TransformerFactory.newInstance().newTransformer();
		tf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		tf.setOutputProperty(OutputKeys.INDENT, "yes");
		Writer out = new StringWriter();
		tf.transform(new DOMSource(xml), new StreamResult(out));
		return out.toString();
	}
}
