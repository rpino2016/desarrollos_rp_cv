package com.example.formBuilder.facade;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.example.formBuilder.dao.AbstractDAO;
import com.example.formBuilder.dao.FormDAO;
import com.example.formBuilder.dao.LegacyDAO;
import com.example.formBuilder.dao.LegacyXFormDAO;
import com.example.formBuilder.dao.ParameterDAO;
import com.example.formBuilder.dao.RequesterDAO;
import com.example.formBuilder.dao.UserDAO;
import com.example.formBuilder.exception.FormBuilderException;
import com.example.formBuilder.model.FormModel;
import com.example.formBuilder.model.LegacyModel;
import com.example.formBuilder.model.ParameterModel;
import com.example.formBuilder.model.RequesterModel;
import com.example.formBuilder.model.UserModel;
import com.example.formBuilder.util.LegacyTypeEnum;
import com.example.formBuilder.util.ResultCodeEnum;
import com.example.formBuilder.util.StateEnum;
import com.example.formBuilder.vo.FormVO;
import com.example.formBuilder.vo.LegacyVO;
import com.example.formBuilder.vo.LegacyXFormVO;
import com.example.formBuilder.vo.ParameterVO;
import com.example.formBuilder.vo.RequesterVO;
import com.example.formBuilder.vo.UserVO;
import com.google.appengine.api.datastore.Text;

/**
 * Clase fachada para el manejo de la persistencia de la aplicación
 * @author rafaelpino
 *
 */
public class PersistenceFacade {
	private FormDAO formDao;
	private ParameterDAO paramDao;
	private UserDAO userDao;
	private LegacyDAO legDao;
	private LegacyXFormDAO legxformDao;
	private RequesterDAO reqtrDao;
	private static PersistenceFacade instance;
	
	public static final int FORM_DAO = 1;
	public static final int PARAM_DAO = 2;
	public static final int LEGACY_DAO = 3;
	public static final int REQUESTER_DAO = 4;
	
	public static PersistenceFacade getInstance() {
		if (instance == null) {
			instance = new PersistenceFacade();
		}
		return instance;
	}
	
	protected PersistenceFacade(){
		this.formDao = new FormDAO();
		this.paramDao = new ParameterDAO();
		this.userDao = new UserDAO();
		this.legDao = new LegacyDAO();
		this.legxformDao = new LegacyXFormDAO();
		this.reqtrDao = new RequesterDAO();
	}

	public void insertForm(FormModel form){
		FormVO record = new FormVO();
		record.setName(form.getName());
		record.setXml(new Text(form.getData()));
		formDao.insert(record);
	}
	
	public List<FormModel> getForms(){
		List<FormVO> recordList = formDao.getForms();
		return resultSetToFormModelList(recordList);
	}
	
	public FormModel getFormById(Long id){
		FormVO record = (FormVO) formDao.getById(FormVO.class, id);
		FormModel model = new FormModel();
		model.setId(record.getFormID().getId());
		model.setName(record.getName());
		model.setData(record.getXml().getValue());
		return model;
	}
	
	public LegacyModel getLegacyById(Long id) {
		LegacyVO record = (LegacyVO) legDao.getById(LegacyVO.class, id);
		LegacyModel model = new LegacyModel();
		model.setId(record.getLegacyID().getId());
		model.setName(record.getName());
		model.setDescription(record.getDescription());
		model.setTypeName(LegacyTypeEnum.valueOf(record.getLegacyType()));
		return model;
	}
	
	public boolean activateRequester(Long requesterId){
		return reqtrDao.activateRequester(requesterId);
	}
	
	public boolean deactivateRequester(Long requesterId){
		return reqtrDao.deactivateRequester(requesterId);
	}
	
	public boolean blockRequester(Long requesterId){
		return reqtrDao.blockRequester(requesterId);
	}
	
	public List<FormModel> getFormIdList(){
		List<FormVO> recordList = formDao.getFormIdList();
		return resultSetToFormModelList(recordList);
	}
	
	public List<ParameterModel> getParameters() {
		List<ParameterVO> recordList = paramDao.getParameters();
		return resultSetToParameterModelList(recordList);
	}
	
	public List<LegacyModel> getLegacies() {
		List<LegacyVO> recordList = legDao.getLegacies();
		return resultSetToLegacyModelList(recordList);
	}
	
	public List<RequesterModel> getRequesters() {
		List<RequesterVO> recordList = reqtrDao.getRequesters();
		return resultSetToRequesterModelList(recordList);
	}
	
	public List<UserModel> getUserByLogin(String login){
		List<UserVO> recordList = userDao.getUserByLogin(login);
		return resultSetToUserModelList(recordList);
	}
	
	public void insertLegacyXForm(Long formId, Long legacyId) throws FormBuilderException {
		FormVO form = (FormVO) formDao.getById(FormVO.class, formId);
		LegacyVO legacy = (LegacyVO) legDao.getById(LegacyVO.class, legacyId);
		if(form != null && legacy != null){
			LegacyXFormVO record = new LegacyXFormVO();
			record.setFormID(form);
			record.setLegacyID(legacy);
			legxformDao.insert(record);
		}else{
			FormBuilderException exception = new FormBuilderException(ResultCodeEnum.NO_DATA);
			exception.setDetail("FormID: "+ formId + " LegacyID: "+ legacyId);
			throw exception;
		}
	}
	
	/**
	 * Método que retorna el DAO de acuerdo al código recibido.
	 * Implementa el patrón Factory
	 * @param daoCode Códigos expuestos de forma estática por esta clase
	 * @return Clase AbstractDAO con las operaciones CRUD
	 */
	public AbstractDAO getAbstractDAO(int daoCode){
		switch (daoCode) {
		case FORM_DAO:
			return this.formDao;
		case PARAM_DAO:
			return this.paramDao;
		case LEGACY_DAO:
			return this.legDao;
		case REQUESTER_DAO:
			return this.reqtrDao;
		default:
			return null;
		}
	}
	
	private List<FormModel> resultSetToFormModelList(List<FormVO> recordList){
		List<FormModel> response= new ArrayList<FormModel>();
		for(FormVO vo : recordList){
			FormModel model = new FormModel();
			model.setId(vo.getFormID().getId());
			model.setName(vo.getName());
			model.setData(vo.getXml().getValue());
			response.add(model);
		}
		return response;
	}
	
	private List<ParameterModel> resultSetToParameterModelList(List<ParameterVO> recordList){
		List<ParameterModel> response= new ArrayList<ParameterModel>();
		for(ParameterVO vo : recordList){
			ParameterModel model = new ParameterModel();
			model.setId(vo.getParamsID().getId());
			model.setName(vo.getName());
			model.setDescription(vo.getDescription());
			model.setValueList(Arrays.asList(vo.getValues().getValue().split(ParameterModel.VALUE_SEPARATOR)));
			response.add(model);
		}
		return response;
	}
	
	private List<UserModel> resultSetToUserModelList(List<UserVO> recordList){
		List<UserModel> response= new ArrayList<UserModel>();
		for(UserVO vo : recordList){
			UserModel model = new UserModel();
			model.setId(vo.getUserID().getId());
			model.setName(vo.getName());
			model.setLogin(vo.getLogin());
			response.add(model);
		}
		return response;
	}

	private List<LegacyModel> resultSetToLegacyModelList(List<LegacyVO> recordList) {
		List<LegacyModel> response= new ArrayList<LegacyModel>();
		for(LegacyVO vo : recordList){
			LegacyModel model = new LegacyModel();
			model.setId(vo.getLegacyID().getId());
			model.setName(vo.getName());
			model.setDescription(vo.getDescription());
			model.setTypeName(LegacyTypeEnum.valueOf(vo.getLegacyType()));
			response.add(model);
		}
		return response;
	}
	
	private List<RequesterModel> resultSetToRequesterModelList(List<RequesterVO> recordList) {
		List<RequesterModel> response= new ArrayList<RequesterModel>();
		for(RequesterVO vo : recordList){
			RequesterModel model = new RequesterModel();
			model.setRequesterID(vo.getRequesterID().getId());
			model.setAge(vo.getAge());
			model.setGender(vo.getGender());
			model.setIdDocument(vo.getIdDocument());
			model.setLastName(vo.getLastName());
			model.setLogin(vo.getLogin());
			model.setName(vo.getName());
			model.setState(vo.getStateId() != null ? vo.getStateId().getName() : StateEnum.INACTIVO.toString() );
			response.add(model);
		}
		return response;
	}
}
