package com.example.formBuilder.util;

public enum ResultCodeEnum {
	  OK(0,"Operación exitosa. "),
	  DATABASE(1,"Error durante la operación. "),
	  NO_DATA(2," No existen registros para los parámetros indicados. "),
	  NO_PARAMS(3," No se recibieron parámetros en la petición. "),
	  NO_ID(4," Debe indicar el ID para realizar la actualización solicitada. "),
	  UNEXPECTED(5," Error inesperado, consulte con el administrador. ");

	private int resultCode;	
	private String name;
	private ResultCodeEnum(int resultCode, String name) {
		this.resultCode = resultCode;
		this.name = name;
	}
	public int getResultCode() {
		return resultCode;
	}
	public void setResultCode(int resultCode) {
		this.resultCode = resultCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}