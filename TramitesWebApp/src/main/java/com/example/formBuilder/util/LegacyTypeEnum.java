package com.example.formBuilder.util;

public enum LegacyTypeEnum {
	  SOAP_SERVICE(0,"SOAP Service","Servicio tipo SOAP"),
	  REST_SERVICE(1,"REST Service","Servicio tipo REST"),
	  DATABASE(2,"Data Base Provider","Proveedor de bases de datos");

	private int legacyTypeID;	
	private String name;
	private String description;
	
	private LegacyTypeEnum(int legacyTypeID, String name, String description) {
		this.legacyTypeID = legacyTypeID;
		this.name = name;
		this.description = description;
	}
	public int getLegacyTypeID() {
		return legacyTypeID;
	}
	public void setLegacyTypeID(int legacyTypeID) {
		this.legacyTypeID = legacyTypeID;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public static String valueOf(int id){
		String response = "";
		for(LegacyTypeEnum val : values()){
			if(val.getLegacyTypeID() == id){
				response = val.getName();
			}
		}
		return response;
	}
}