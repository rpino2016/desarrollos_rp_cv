package com.example.formBuilder.util;

public enum StateEnum {
	  ACTIVO(0,"Activo"),
	  INACTIVO(1,"Inactivo"),
	  BLOQUEADO(2,"Bloqueado");

	private int stateID;	
	private String name;
	
	private StateEnum(int stateID, String name) {
		this.stateID = stateID;
		this.name = name;
	}
	public int getStateID() {
		return stateID;
	}
	public void setStateID(int stateID) {
		this.stateID = stateID;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return this.name;
	}
	
	
}