package com.example.formBuilder.util;

import java.lang.reflect.Field;

public class ReflectionUtil {

	public boolean copyFieldValues(Object origin, Object target) throws Exception{
		boolean copied = false;
		if(origin.getClass().getName().equalsIgnoreCase(target.getClass().getName())){
			Field[] originFields = origin.getClass().getDeclaredFields();
			// Copia cada campo del objeto origen al destino
			for(Field originField : originFields){
				Field targetField = target.getClass().getDeclaredField(originField.getName());
				originField.setAccessible(true);
				targetField.setAccessible(true);
				targetField.set(targetField, originField.get(originField));
			}
			copied = true;
		}
		return copied;
	}
}
