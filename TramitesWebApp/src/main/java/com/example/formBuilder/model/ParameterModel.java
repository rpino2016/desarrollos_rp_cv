package com.example.formBuilder.model;

import java.util.List;

public class ParameterModel {
	private Long id;
	private String name;
	private String description;
	private List<String> valueList;
	public static final String VALUE_SEPARATOR=";";
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<String> getValueList() {
		return valueList;
	}
	public void setValueList(List<String> valueList) {
		this.valueList = valueList;
	}
	
}
