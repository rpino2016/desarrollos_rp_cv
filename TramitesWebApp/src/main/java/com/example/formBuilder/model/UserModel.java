package com.example.formBuilder.model;

public class UserModel {
	private Long id;
	private String name;
	private String login;
	private boolean active;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	@Override
	public String toString(){
		StringBuilder b = new StringBuilder();
		b.append("\n  Login: " + this.login);
		b.append("\n  Name: "  + this.name);
		return b.toString();
	}
	
}
