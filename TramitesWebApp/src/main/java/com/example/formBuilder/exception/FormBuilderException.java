package com.example.formBuilder.exception;

import com.example.formBuilder.util.ResultCodeEnum;

public class FormBuilderException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int code;
	private String message;
	private String detail;
	
	public FormBuilderException(int code, String message, String detail) {
		super();
		this.code = code;
		this.message = message;
		this.detail = detail;
	}
	public FormBuilderException(ResultCodeEnum code){
		this.code = code.getResultCode();
		this.message = code.getName();
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	
}
