package com.example.formBuilder.vo;

import java.io.Serializable;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;

/**
 * Clase para almacenar la información de los sistemas heredados a los que se enviarán las solicitudes de 
 * trámite. 
 * Almacena:
 *  - Name: Identificador del legado
 *  - Description: Descripción del tipo de legado
 *  - LegacyType: Tipo de legado a configurar
 *  - 
 * @author rafaelpino
 *
 */
@PersistenceCapable(table="Legacy")
public class LegacyVO  implements  Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key legacyID;
	@Persistent
	private String name;
	@Persistent
	private String description;
	@Persistent
	private int legacyType;
	@Persistent
	private LegacyConfigurationVO legacyConfiguration;
	public Key getLegacyID() {
		return legacyID;
	}
	public void setLegacyID(Key legacyID) {
		this.legacyID = legacyID;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getLegacyType() {
		return legacyType;
	}
	public void setLegacyType(int legacyType) {
		this.legacyType = legacyType;
	}
	public LegacyConfigurationVO getLegacyConfiguration() {
		return legacyConfiguration;
	}
	public void setLegacyConfiguration(LegacyConfigurationVO legacyConfiguration) {
		this.legacyConfiguration = legacyConfiguration;
	}
}
