package com.example.formBuilder.vo;

import java.io.Serializable;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;

/**
 * Clase con los diferentes estados manejados por la aplicación
 * Tiene su equivalente Enum para evitar consultas a la BD para configurar estados 
 * Almacena:
 *  - Id: Identificador del estado
 *  - Name: Nombre del estado
 * @author rafaelpino
 *
 */
@PersistenceCapable(table="State")
public class StateVO  implements  Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key stateID;
	@Persistent
	private String name;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Key getStateID() {
		return stateID;
	}
	public void setStateID(Key stateID) {
		this.stateID = stateID;
	}
}
