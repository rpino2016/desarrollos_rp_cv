package com.example.formBuilder.vo;

import java.io.Serializable;
import java.util.Date;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Text;

/**
 * Clase para almacenar los datos de una notificación
 * Almacena:
 *  - Date: Fecha de la notificación
 *  - notSubject: Asunto de la notificación (Texto corto)
 *  - notAuthor: Correo electrónico del administrador que crea la notificación (De las credenciales de OAuth)
 *  - notMessage: Contenido de la notificación (Texto largo)
 * @author rafaelpino
 *
 */
@PersistenceCapable(table="Notification")
public class NotificationVO  implements  Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key notID;
	@Persistent
	private Date notDate;
	@Persistent
	private String notSubject;
	@Persistent
	private String notAuthor;
	@Persistent
	private Text notMessage;
	public Key getNotID() {
		return notID;
	}
	public void setNotID(Key notID) {
		this.notID = notID;
	}
	public Date getNotDate() {
		return notDate;
	}
	public void setNotDate(Date notDate) {
		this.notDate = notDate;
	}
	public String getNotSubject() {
		return notSubject;
	}
	public void setNotSubject(String notSubject) {
		this.notSubject = notSubject;
	}
	public String getNotAuthor() {
		return notAuthor;
	}
	public void setNotAuthor(String notAuthor) {
		this.notAuthor = notAuthor;
	}
	public Text getNotMessage() {
		return notMessage;
	}
	public void setNotMessage(Text notMessage) {
		this.notMessage = notMessage;
	}
}
