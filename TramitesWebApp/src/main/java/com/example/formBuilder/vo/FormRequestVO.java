package com.example.formBuilder.vo;

import java.io.Serializable;
import java.util.Date;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;

/**
 * Clase para almacenar la petición asociada a un formulario existente
 * Cada petición es única pero un formulario puede tener muchas peticiones asociadas.
 * Almacena:
 *  - Id: Identificador de la solicitud
 *  - FormId: Identificador del formulario utilizado para la solicitud
 *  - FormResponseId: Respuesta recibida a la solicitud
 *  - RequesterId: Identificador del solicitante
 *  - RequestDate: Fecha de la solicitud
 * @author rafaelpino
 *
 */
@PersistenceCapable(table="FormRequest")
public class FormRequestVO  implements  Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key formRequestID;
	@Persistent
	private FormVO formId;
	@Persistent
	private FormResponseVO formResponseId;
	@Persistent
	private RequesterVO requesterId;
	@Persistent
	private Date requestDate;
	public Key getFormRequestID() {
		return formRequestID;
	}
	public void setFormRequestID(Key formRequestID) {
		this.formRequestID = formRequestID;
	}
	public FormVO getFormId() {
		return formId;
	}
	public void setFormId(FormVO formId) {
		this.formId = formId;
	}
	public FormResponseVO getFormResponseId() {
		return formResponseId;
	}
	public void setFormResponseId(FormResponseVO formResponseId) {
		this.formResponseId = formResponseId;
	}
	public RequesterVO getRequesterId() {
		return requesterId;
	}
	public void setRequesterId(RequesterVO requesterId) {
		this.requesterId = requesterId;
	}
	public Date getRequestDate() {
		return requestDate;
	}
	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

}
