package com.example.formBuilder.vo;

import java.io.Serializable;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Text;

/**
 * Clase para almacenar los datos paramétricos
 * Almacena:
 *  - Name: Identificador de la paramétrica
 *  - Description: Descripción del conjunto de parámetros
 *  - Value: Listado separado por ; de los valores posibles
 * @author rafaelpino
 *
 */
@PersistenceCapable(table="Parameter")
public class ParameterVO implements  Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key paramsID;
	@Persistent
	private String name;
	@Persistent
	private String description;
	@Persistent
	private Text values;
	public Key getParamsID() {
		return paramsID;
	}
	public void setParamsID(Key paramsID) {
		this.paramsID = paramsID;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Text getValues() {
		return values;
	}
	public void setValues(Text values) {
		this.values = values;
	}
}
