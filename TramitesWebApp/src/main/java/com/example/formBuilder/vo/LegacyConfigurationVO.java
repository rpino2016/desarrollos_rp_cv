package com.example.formBuilder.vo;

import java.io.Serializable;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;

/**
 * Clase para almacenar la información de  configuración de los sistemas heredados a los que se enviarán las solicitudes de 
 * trámite. 
 * Almacena:
 *  - Name: Identificador de la configuración del legado
 *  - Url: Url para acceder al proveedor de acuerdo al tipo de protocolo ejem, jbc:<host>:<port>//<database>
 *  - Timeout: Tiempo de espera para obtener una respuesta del legado
 *  - User: Usuario a utilizar para autenticarse en el legado
 *  - Password: Contraseña para autenticación
 *  - ActionName: Función, procedimiento almacenado u operación que atiende la solicitud.
 *  - ResponseCodeMapping: Nombre del elemento que contiene  el código de respuesta del legado.
 *  - ResponseMessageMapping: Nombre del elemento que contiene el mensaje de respueta del legado.
 *  - ResponseCaseNumberMapping: Nombre del elemento que contiene el número de caso de la solicitud en la respuesta del legado.
 * @author rafaelpino
 *
 */
@PersistenceCapable(table="LegacyConfiguration")
public class LegacyConfigurationVO  implements  Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key legacyConfigurationID;
	@Persistent
	private String name;
	@Persistent
	private String url;
	@Persistent
	private int timeout;
	@Persistent
	private String user;
	@Persistent
	private String password;
	@Persistent
	private String actionName;
	@Persistent
	private String responseCodeMapping;
	@Persistent
	private String responseMessageMapping;
	@Persistent
	private String responseCaseNumberMapping;
	public Key getLegacyConfigurationID() {
		return legacyConfigurationID;
	}
	public void setLegacyConfigurationID(Key legacyConfigurationID) {
		this.legacyConfigurationID = legacyConfigurationID;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getTimeout() {
		return timeout;
	}
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getActionName() {
		return actionName;
	}
	public void setActionName(String actionName) {
		this.actionName = actionName;
	}
	public String getResponseCodeMapping() {
		return responseCodeMapping;
	}
	public void setResponseCodeMapping(String responseCodeMapping) {
		this.responseCodeMapping = responseCodeMapping;
	}
	public String getResponseMessageMapping() {
		return responseMessageMapping;
	}
	public void setResponseMessageMapping(String responseMessageMapping) {
		this.responseMessageMapping = responseMessageMapping;
	}
	public String getResponseCaseNumberMapping() {
		return responseCaseNumberMapping;
	}
	public void setResponseCaseNumberMapping(String responseCaseNumberMapping) {
		this.responseCaseNumberMapping = responseCaseNumberMapping;
	}
}
