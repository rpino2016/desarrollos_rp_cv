package com.example.formBuilder.vo;

import java.io.Serializable;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;

/**
 * Clase para almacenar la información de los usuarios registrados por un cliente. 
 * Almacena:
 *  - Name: Identificador del formulario
 *  - login: Correo electrónico con el que accede el usuario desginado
 * @author rafaelpino
 *
 */
@PersistenceCapable(table="User")
public class UserVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key userID;
	@Persistent
	private String name;
	@Persistent
	private String login;
	public Key getUserID() {
		return userID;
	}
	public void setUserID(Key userID) {
		this.userID = userID;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	
}
