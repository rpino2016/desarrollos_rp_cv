package com.example.formBuilder.vo;

import java.io.Serializable;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;

/**
 * Clase para almacenar la información de los clientes que contraten el servicio. 
 * Almacena:
 *  - Id: Identificador del cliente (NIT)
 *  - Name: Nombre o razón social
 *  - StateId: Estado del cliente (Activo, Inactivo, Bloqueado)
 *  - BillingAddress: Dirección de facturación
 *  - ContactNumber: Número de contacto del cliente
 * @author rafaelpino
 *
 */
@PersistenceCapable(table="Form")
public class CustomerVO  implements  Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key customerID;
	@Persistent
	private String name;
	@Persistent
	private StateVO stateId;
	@Persistent
	private String billingAddress;
	@Persistent
	private Long contactNumber;
	public Key getCustomerID() {
		return customerID;
	}
	public void setCustomerID(Key customerID) {
		this.customerID = customerID;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public StateVO getStateId() {
		return stateId;
	}
	public void setStateId(StateVO stateId) {
		this.stateId = stateId;
	}
	public String getBillingAddress() {
		return billingAddress;
	}
	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}
	public Long getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(Long contactNumber) {
		this.contactNumber = contactNumber;
	}
}
