package com.example.formBuilder.vo;

import java.io.Serializable;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;

/**
 * Clase para almacenar la relación entre formularios y sistemas legado 
 * Almacena:
 *  - Name: Identificador del tipo de legado
 *  - Description: Descripción del tipo de legado
 *  - LegacyType: Tipo de legado a configurar
 *  - 
 * @author rafaelpino
 *
 */
@PersistenceCapable(table="LegacyXForm")
public class LegacyXFormVO  implements  Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key legacyXFormID;
	@Persistent
	private FormVO formID;
	@Persistent
	private LegacyVO legacyID;
	public Key getLegacyXFormID() {
		return legacyXFormID;
	}
	public void setLegacyXFormID(Key legacyXFormID) {
		this.legacyXFormID = legacyXFormID;
	}
	public FormVO getFormID() {
		return formID;
	}
	public void setFormID(FormVO formID) {
		this.formID = formID;
	}
	public LegacyVO getLegacyID() {
		return legacyID;
	}
	public void setLegacyID(LegacyVO legacyID) {
		this.legacyID = legacyID;
	}
}
