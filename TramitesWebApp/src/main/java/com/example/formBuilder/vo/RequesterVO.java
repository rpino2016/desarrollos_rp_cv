package com.example.formBuilder.vo;

import java.io.Serializable;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import javax.persistence.FetchType;

import com.google.appengine.api.datastore.Key;

/**
 * Clase para almacenar la información del solicitante de un trámite
 * Almacena:
 *  - Id: Identificador del solicitante
 *  - Name: Nombres de la persona
 *  - LastName: Apellidos de la persona
 *  - IdDocument: Número de identificación del solicitante
 *  - Age: Edad 
 *  - Gender: Sexo (M- Masculino -F Femenino -O Otro)
 *  - Login: Login del solicitante
 *  - State: Estado del solicitante (0- Activo , 1- Inactivo, 2- Bloqueado)
 * @author rafaelpino
 *
 */
@PersistenceCapable(table="Requester")
public class RequesterVO  implements  Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key requesterID;
	@Persistent
	private String name;
	@Persistent
	private String lastName;
	@Persistent
	private Long idDocument;
	@Persistent
	private int age;
	@Persistent
	private char gender;
	@Persistent
	private String login;
	@Persistent(defaultFetchGroup="true")
	private StateVO stateId;
	public Key getRequesterID() {
		return requesterID;
	}
	public void setRequesterID(Key requesterID) {
		this.requesterID = requesterID;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Long getIdDocument() {
		return idDocument;
	}
	public void setIdDocument(Long idDocument) {
		this.idDocument = idDocument;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public char getGender() {
		return gender;
	}
	public void setGender(char gender) {
		this.gender = gender;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public StateVO getStateId() {
		return stateId;
	}
	public void setStateId(StateVO stateId) {
		this.stateId = stateId;
	}
}
