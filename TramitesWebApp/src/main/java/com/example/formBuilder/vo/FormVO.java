package com.example.formBuilder.vo;

import java.io.Serializable;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Text;

/**
 * Clase para almacenar la información de los formularios. 
 * Almacena:
 *  - Name: Identificador del formulario
 *  - XML: Cadena xml del formulario
 * @author rafaelpino
 *
 */
@PersistenceCapable(table="Form")
public class FormVO  implements  Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key formID;
	@Persistent
	private String name;
	@Persistent
	private Text xml;
	public Key getFormID() {
		return formID;
	}
	public void setFormID(Key formID) {
		this.formID = formID;
	}	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Text getXml() {
		return xml;
	}
	public void setXml(Text xml) {
		this.xml = xml;
	}
}
