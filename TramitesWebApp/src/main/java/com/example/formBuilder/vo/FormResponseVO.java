package com.example.formBuilder.vo;

import java.io.Serializable;
import java.util.Date;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;

/**
 * Clase para almacenar la respuesta recibida para una petición de un formulario
 * Almacena:
 *  - Id: Identificador del formulario
 *  - ResponseDate: Fecha en la que se recibió respuesta de la solicitud de trámite
 *  - ResultCode: Código de respuesta recibido
 *  - ResultMessage: Mensaje de respuesta recibido
 *  - CaseNumber: Número de caso creado en la plataforma que procesa la solicitud
 * @author rafaelpino
 *
 */
@PersistenceCapable(table="FormResponse")
public class FormResponseVO  implements  Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key formResponseID;
	@Persistent
	private Date responseDate;
	@Persistent
	private String resultCode;
	@Persistent
	private String resultMessage;
	@Persistent
	private String caseNumber;
	public Key getFormResponseID() {
		return formResponseID;
	}
	public void setFormResponseID(Key formResponseID) {
		this.formResponseID = formResponseID;
	}
	public Date getResponseDate() {
		return responseDate;
	}
	public void setResponseDate(Date responseDate) {
		this.responseDate = responseDate;
	}
	public String getResultCode() {
		return resultCode;
	}
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	public String getResultMessage() {
		return resultMessage;
	}
	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}
	public String getCaseNumber() {
		return caseNumber;
	}
	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}
}
