package com.example.formBuilder.dao;

import java.lang.reflect.Field;
import java.util.logging.Logger;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.Transaction;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;

import com.google.appengine.api.datastore.Key;

/***
 * Clase abstracta para las operaciones CRUD
 * @author rafaelpino
 *
 */
public abstract class AbstractDAO {
	public final static Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());
	/**
	 * Método abstracto para insertar objetos en la base de datos
	 * @param record Objeto a insertar
	 */
	public void insert(Object record){
		PersistenceManager pm = JDOHelper.getPersistenceManagerFactory("transactions-optional").getPersistenceManager();
		try {
		    pm.makePersistent(record);
		} catch(Exception e){
			LOGGER.severe("Error durante la operación con base de datos: " + ExceptionUtils.getRootCauseMessage(e));
		}finally {
			pm.close();
		}
	}
	
	/**
	 * Método abstracto para eliminar objetos en la base de datos
	 * @param record Objeto a eliminar
	 */
	public void delete(Object record) {
		PersistenceManager pm = JDOHelper.getPersistenceManagerFactory("transactions-optional").getPersistenceManager();
		try {
			 pm.deletePersistent(record);
		} catch(Exception e){
			LOGGER.severe("Error durante la operación con base de datos: " + ExceptionUtils.getRootCauseMessage(e));
		}finally {
			pm.close();
		}
	}
	
	/**
	 * Método abstracto para consultar objetos en la base de datos
	 * @param record Objeto a consultar
	 */
	public void getAll(Object record) {
		PersistenceManager pm = JDOHelper.getPersistenceManagerFactory("transactions-optional").getPersistenceManager();
		try {
			pm.retrieveAll(record);
		} catch(Exception e){
			LOGGER.severe("Error durante la operación con base de datos: " + ExceptionUtils.getRootCauseMessage(e));
		}finally {
			pm.close();
		}
	}
	
	/**
	 * Método abstraco para actualizar un objeto almacenado de acuerdo a los cambios detectados de forma reflectiva
	 * @param toChangeRecord Objeto con los cambios a ejecutar
	 * @param id Identificador del registro que va a ser actualizado
	 */
	public void update(Object toChangeRecord, Long id){
		PersistenceManager pm = JDOHelper.getPersistenceManagerFactory("transactions-optional").getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		try {
			tx.begin();
			// Obtiene el objeto existente según su ID
			Object persisted = pm.getObjectById(toChangeRecord.getClass(),id);
			// Modifica cada campo excepto el ID, ese debe dejarse igual
			Field[] fields = persisted.getClass().getDeclaredFields();
			for(Field field : fields){
				if(!java.lang.reflect.Modifier.isStatic(field.getModifiers()) && !java.lang.reflect.Modifier.isProtected(field.getModifiers()) && !field.getType().getName().equals(Key.class.getName())){ //Verifica que no sea de tipo Key
					field.setAccessible(true);
					Field toChangeField = toChangeRecord.getClass().getDeclaredField(field.getName());
					toChangeField.setAccessible(true);
					Object toChangeValue = toChangeField.get(toChangeRecord);
					Object persistedValue = field.get(persisted);
					if(!persistedValue.equals(toChangeValue)){
						String methodName = "set" + StringUtils.capitalize(field.getName());
						// Configura el nuevo valor en el objeto persistido y refleja el cambio una vez hace commit
						persisted.getClass().getMethod(methodName, field.getType()).invoke(persisted, toChangeValue);
					}
					field.setAccessible(false);
					toChangeField.setAccessible(false);
				}
			}
			tx.commit();
		}catch (Exception e){
			LOGGER.severe("Error durante la operación con base de datos: " + ExceptionUtils.getRootCauseMessage(e));
		        if (tx.isActive()){
		            tx.rollback();
		        }
		} finally {
			pm.close();
		}
	}
	
	/**
	 * Método abstracto para eliminar un objeto por su Id
	 * @param toDeleteClass Clase del objeto a buscar
	 * @param id
	 */
	public void deleteById(Class toDeleteClass, Long id){
		PersistenceManager pm = JDOHelper.getPersistenceManagerFactory("transactions-optional").getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		try {
			tx.begin();
			// Obtiene el objeto existente según su ID
			Object persisted = pm.getObjectById(toDeleteClass,id);
			pm.deletePersistent(persisted); //Elimina el objeto
			tx.commit();
		}catch (Exception e){
			LOGGER.severe("Error durante la operación con base de datos: " + ExceptionUtils.getRootCauseMessage(e));
		        if (tx.isActive()){
		            tx.rollback();
		        }
		} finally {
			pm.close();
		}
	}
	
	/**
	 * Método abstracto para consultar un objeto por su Id
	 * @param toGetClass
	 * @param id
	 * @return
	 */
	public Object getById(Class toGetClass, Long id){
		PersistenceManager pm = JDOHelper.getPersistenceManagerFactory("transactions-optional").getPersistenceManager();
		Object persisted = null;
		try {
			// Obtiene el objeto existente según su ID
			persisted  = pm.getObjectById(toGetClass,id);
		}catch (Exception e){
			LOGGER.severe("Error durante la operación con base de datos: " + ExceptionUtils.getRootCauseMessage(e));
		} finally {
			pm.close();
		}
		return persisted;
	}
}
