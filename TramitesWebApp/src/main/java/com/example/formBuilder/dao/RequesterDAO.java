package com.example.formBuilder.dao;


import java.util.ArrayList;
import java.util.List;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

import org.apache.commons.lang3.exception.ExceptionUtils;

import com.example.formBuilder.util.StateEnum;
import com.example.formBuilder.vo.RequesterVO;
import com.example.formBuilder.vo.StateVO;

public class RequesterDAO extends AbstractDAO{
	
	@SuppressWarnings("unchecked")
	public List<RequesterVO> getRequesters() {
		PersistenceManager pm = JDOHelper.getPersistenceManagerFactory("transactions-optional").getPersistenceManager();
		List<RequesterVO> resultSet = new ArrayList<RequesterVO>();
		Query q = pm.newQuery("select from " + RequesterVO.class.getName());
		try {
			resultSet = (List<RequesterVO>) q.execute();
			resultSet.size();// BUG, esto previene error en la consulta
		} finally {
			q.closeAll();
			pm.close();
		}
		return resultSet;
	}
	
	public boolean activateRequester(Long requesterId) {
		PersistenceManager pm = JDOHelper.getPersistenceManagerFactory("transactions-optional").getPersistenceManager();
		boolean activated = false;
		Transaction tx = pm.currentTransaction();	
		Query q = pm.newQuery("select from " + StateVO.class.getName() + " where name == '" + StateEnum.ACTIVO.getName() + "'");
		q.setUnique(true);
		try {
			tx.begin();
			RequesterVO requester = pm.getObjectById(RequesterVO.class, requesterId);
			StateVO activeState = (StateVO) q.execute();
			// Si no existe el estado lo crea en las tablas
			if(activeState==null){
				activeState = new StateVO();
				activeState.setName(StateEnum.ACTIVO.getName());
			}
			// Configura el estado activo para el solicitante
			requester.setStateId(activeState);
			pm.makePersistent(requester);
			tx.commit();
			activated = true;
		}catch(Exception e){
			LOGGER.info(" Error durante la activación del solicitante. Detalle: " + ExceptionUtils.getRootCauseMessage(e));
			activated = false;
		}finally {
			q.closeAll();
			if(tx.isActive()){
				tx.rollback();
			}
			pm.close();
		}
		return activated;
	}

	public boolean deactivateRequester(Long requesterId) {
		PersistenceManager pm = JDOHelper.getPersistenceManagerFactory("transactions-optional").getPersistenceManager();
		boolean deactivated = false;
		Transaction tx = pm.currentTransaction();	
		Query q = pm.newQuery("select from " + StateVO.class.getName() + " where name == '" + StateEnum.INACTIVO.getName() + "'");
		q.setUnique(true);
		try {
			tx.begin();
			RequesterVO requester = pm.getObjectById(RequesterVO.class, requesterId);
			StateVO deactivedState = (StateVO) q.execute();
			// Si no existe el estado lo crea en las tablas
			if(deactivedState==null){
				deactivedState = new StateVO();
				deactivedState.setName(StateEnum.INACTIVO.getName());
			}
			// Configura el estado activo para el solicitante
			requester.setStateId(deactivedState);
			pm.makePersistent(requester);
			tx.commit();
			deactivated = true;
		}catch(Exception e){
			LOGGER.info(" Error durante la desactivación del solicitante. Detalle: " + ExceptionUtils.getRootCauseMessage(e));
			deactivated = false;
		}finally {
			q.closeAll();
			if(tx.isActive()){
				tx.rollback();
			}
			pm.close();
		}
		return deactivated;
	}

	public boolean blockRequester(Long requesterId) {
		PersistenceManager pm = JDOHelper.getPersistenceManagerFactory("transactions-optional").getPersistenceManager();
		boolean blocked = false;
		Transaction tx = pm.currentTransaction();	
		Query q = pm.newQuery("select from " + StateVO.class.getName() + " where name == '" + StateEnum.BLOQUEADO.getName() + "'");
		q.setUnique(true);
		try {
			tx.begin();
			RequesterVO requester = pm.getObjectById(RequesterVO.class, requesterId);
			StateVO deactivedState = (StateVO) q.execute();
			// Si no existe el estado lo crea en las tablas
			if(deactivedState==null){
				deactivedState = new StateVO();
				deactivedState.setName(StateEnum.BLOQUEADO.getName());
			}
			// Configura el estado activo para el solicitante
			requester.setStateId(deactivedState);
			pm.makePersistent(requester);
			tx.commit();
			blocked = true;
		}catch(Exception e){
			LOGGER.info(" Error durante el bloqueo del solicitante. Detalle: " + ExceptionUtils.getRootCauseMessage(e));
			blocked = false;
		}finally {
			q.closeAll();
			if(tx.isActive()){
				tx.rollback();
			}
			pm.close();
		}
		return blocked;
	}

}
