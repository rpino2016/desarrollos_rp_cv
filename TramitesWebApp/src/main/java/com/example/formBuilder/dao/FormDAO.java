package com.example.formBuilder.dao;


import java.util.ArrayList;
import java.util.List;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import com.example.formBuilder.vo.FormVO;

public class FormDAO extends AbstractDAO{
	
	@SuppressWarnings("unchecked")
	public List<FormVO> getForms() {
		PersistenceManager pm = JDOHelper.getPersistenceManagerFactory("transactions-optional").getPersistenceManager();
		List<FormVO> resultSet = new ArrayList<FormVO>();
		Query q = pm.newQuery("select from " + FormVO.class.getName());
		try {
			resultSet = (List<FormVO>) q.execute();
			resultSet.size();// BUG, esto previene error en la consulta
		} finally {
			q.closeAll();
			pm.close();
		}
		return resultSet;
	}
	
	@SuppressWarnings("unchecked")
	public List<FormVO> getFormIdList() {
		PersistenceManager pm = JDOHelper.getPersistenceManagerFactory("transactions-optional").getPersistenceManager();
		List<FormVO> resultSet = new ArrayList<FormVO>();
		Query q = pm.newQuery("select from " + FormVO.class.getName());
		try {
			resultSet = (List<FormVO>) q.execute();
			resultSet.size();// BUG, esto previene error en la consulta
		} finally {
			q.closeAll();
			pm.close();
		}
		return resultSet;
	}

}
