package com.example.formBuilder.dao;


import java.util.ArrayList;
import java.util.List;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import com.example.formBuilder.vo.LegacyVO;

public class LegacyDAO extends AbstractDAO{
	
	@SuppressWarnings("unchecked")
	public List<LegacyVO> getLegacies() {
		PersistenceManager pm = JDOHelper.getPersistenceManagerFactory("transactions-optional").getPersistenceManager();
		List<LegacyVO> resultSet = new ArrayList<LegacyVO>();
		Query q = pm.newQuery("select from " + LegacyVO.class.getName());
		try {
			resultSet = (List<LegacyVO>) q.execute();
			resultSet.size();// BUG, esto previene error en la consulta
		} finally {
			q.closeAll();
			pm.close();
		}
		return resultSet;
	}
	
}
