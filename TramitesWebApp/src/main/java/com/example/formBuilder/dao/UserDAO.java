package com.example.formBuilder.dao;


import java.util.ArrayList;
import java.util.List;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import com.example.formBuilder.vo.UserVO;

public class UserDAO extends AbstractDAO{
	
	@SuppressWarnings("unchecked")
	public List<UserVO> getUsers() {
		PersistenceManager pm = JDOHelper.getPersistenceManagerFactory("transactions-optional").getPersistenceManager();
		List<UserVO> resultSet = new ArrayList<UserVO>();
		Query q = pm.newQuery("select from " + UserVO.class.getName());
		try {
			resultSet = (List<UserVO>) q.execute();
			resultSet.size();// BUG, esto previene error en la consulta
		} finally {
			q.closeAll();
			pm.close();
		}
		return resultSet;
	}
	
	@SuppressWarnings("unchecked")
	public List<UserVO> getUserByLogin(String login) {
		PersistenceManager pm = JDOHelper.getPersistenceManagerFactory("transactions-optional").getPersistenceManager();
		List<UserVO> resultSet = new ArrayList<UserVO>();
		Query q = pm.newQuery("select from " + UserVO.class.getName() + " where login == '"+ login+"'");
		
		try {
			resultSet = (List<UserVO>) q.execute();
			resultSet.size();// BUG, esto previene error en la consulta
		} finally {
			q.closeAll();
			pm.close();
		}
		return resultSet;
	}

}
