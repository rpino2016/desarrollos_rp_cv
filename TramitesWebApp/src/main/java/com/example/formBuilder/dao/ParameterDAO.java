package com.example.formBuilder.dao;


import java.util.ArrayList;
import java.util.List;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import com.example.formBuilder.vo.ParameterVO;

public class ParameterDAO extends AbstractDAO{
	
	@SuppressWarnings("unchecked")
	public List<ParameterVO> getParameters() {
		PersistenceManager pm = JDOHelper.getPersistenceManagerFactory("transactions-optional").getPersistenceManager();
		List<ParameterVO> resultSet = new ArrayList<ParameterVO>();
		Query q = pm.newQuery("select from " + ParameterVO.class.getName());
		try {
			resultSet = (List<ParameterVO>) q.execute();
			resultSet.size();// BUG, esto previene error en la consulta
			
		} finally {
			q.closeAll();
			pm.close();
		}
		return resultSet;
	}
	

}
