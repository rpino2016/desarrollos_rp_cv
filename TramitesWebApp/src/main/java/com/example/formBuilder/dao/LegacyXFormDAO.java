package com.example.formBuilder.dao;


import java.util.ArrayList;
import java.util.List;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import com.example.formBuilder.vo.LegacyXFormVO;

public class LegacyXFormDAO extends AbstractDAO{
	
	@SuppressWarnings("unchecked")
	public List<LegacyXFormVO> getLegaciesXForm() {
		PersistenceManager pm = JDOHelper.getPersistenceManagerFactory("transactions-optional").getPersistenceManager();
		List<LegacyXFormVO> resultSet = new ArrayList<LegacyXFormVO>();
		Query q = pm.newQuery("select from " + LegacyXFormVO.class.getName());
		try {
			resultSet = (List<LegacyXFormVO>) q.execute();
			resultSet.size();// BUG, esto previene error en la consulta
		} finally {
			q.closeAll();
			pm.close();
		}
		return resultSet;
	}

}
