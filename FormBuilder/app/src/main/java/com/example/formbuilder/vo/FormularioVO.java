package com.example.formbuilder.vo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rafaelpino on 5/7/16.
 */
public class FormularioVO implements Parcelable {
    private Long id;
    private String nombre;
    private List<FieldVO> fields;

    public FormularioVO() {
        super();
        this.fields = new ArrayList<FieldVO>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<FieldVO> getFields() {
        return fields;
    }

    public void setFields(List<FieldVO> fields) {
        this.fields = fields;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(nombre);
        if(fields==null){
            fields = new ArrayList<FieldVO>();
        }
        dest.writeTypedList(fields);
    }

    // The following methods that are required for using Parcelable
    private FormularioVO(Parcel in) {
        // This order must match the order in writeToParcel()
        id = in.readLong();
        nombre = in.readString();
        if (fields == null) {
            fields = new ArrayList<FieldVO>();
        }
        try {
            in.readTypedList(fields, FieldVO.CREATOR);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Continue doing this for the rest of your member data
    }

    // Just cut and paste this for now
    public static final Parcelable.Creator<FormularioVO> CREATOR = new Parcelable.Creator<FormularioVO>() {
        public FormularioVO createFromParcel(Parcel in) {
            return new FormularioVO(in);
        }

          public FormularioVO[] newArray(int size) {
            return new FormularioVO[size];
        }
    };
}
