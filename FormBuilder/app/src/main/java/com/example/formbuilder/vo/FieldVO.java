package com.example.formbuilder.vo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by rafaelpino on 5/7/16.
 */
public class FieldVO implements Parcelable{
    private String label;
    private String name;
    private String type;

    public FieldVO(){
        super();
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(label);
        dest.writeString(name);
        dest.writeString(type);
    }

    // The following methods that are required for using Parcelable
    private FieldVO(Parcel in) {
        // This order must match the order in writeToParcel()
        label = in.readString();
        name = in.readString();
        type = in.readString();

        // Continue doing this for the rest of your member data
    }

    // Just cut and paste this for now
    public static final Parcelable.Creator<FieldVO> CREATOR = new Parcelable.Creator<FieldVO>() {
        public FieldVO createFromParcel(Parcel in) {
            return new FieldVO(in);
        }

        public FieldVO[] newArray(int size) {
            return new FieldVO[size];
        }
    };
}
