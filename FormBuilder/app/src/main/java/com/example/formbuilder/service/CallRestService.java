package com.example.formbuilder.service;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.formbuilder.vo.FieldVO;
import com.example.formbuilder.vo.FormularioVO;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.ByteArrayInputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * Created by rafaelpino on 5/2/16.
 */
public class CallRestService {

    public static final int FORM_LIST_TYPE=1;
    public static final int DETAILED_LIST_TYPE=2;

    /***
     * Metodo para invocar el servicio REST
     * @param url
     * @param params
     * @param serviceHandler La actividad de retorno
     */
    public void invokeWS(String url, RequestParams params, final IServiceHandler serviceHandler, final int requestType) {
        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();

        AsyncHttpResponseHandler responseHandler = new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                String response = new String(bytes);
                Log.i("Response: ",response);
                if(serviceHandler instanceof IServiceHandler){
                    serviceHandler.onWSResponse(parseXML(response), requestType);
                }
            }

            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                if(serviceHandler instanceof  IServiceHandler){
                    serviceHandler.onWSError(i);
                }

            }
        };
        client.get(url, params,responseHandler);

    }


    public CallRestService() {
    }

    private ArrayList<FormularioVO> parseXML(String response) {

        ArrayList<FormularioVO> formularios = new ArrayList<FormularioVO>();
        ArrayList<FieldVO> fieldsList = new ArrayList<FieldVO>();

        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(new ByteArrayInputStream(response.getBytes()));
            doc.getDocumentElement().normalize();

            NodeList roots = doc.getElementsByTagName("Formulario");
            for (int i = 0; i < roots.getLength(); i++) {
                Node rootNode = roots.item(i);
                if (rootNode.getNodeType() == Node.ELEMENT_NODE) {
                    FormularioVO formularioVO = new FormularioVO();
                    String id = ((Element) rootNode).getAttribute("Id");
                    String name = ((Element) rootNode).getAttribute("Nombre");
                    if (id != null) {
                        formularioVO.setId(Long.parseLong(id));
                    }
                    formularioVO.setNombre(name);
                    NodeList fields = ((Element) rootNode).getElementsByTagName("field");
                    for (int f = 0; f < fields.getLength(); f++) {
                        Node fieldNode = fields.item(f);
                        FieldVO field = new FieldVO();
                        Node label = fieldNode.getAttributes().getNamedItem("label");
                        Node nam = fieldNode.getAttributes().getNamedItem("name");
                        Node typ = fieldNode.getAttributes().getNamedItem("type");
                        field.setLabel(label!=null? label.getNodeValue() : "");
                        field.setName(nam!=null? nam.getNodeValue() : "");
                        field.setType(typ!=null? typ.getNodeValue() : "");
                        fieldsList.add(field);
                    }
                    formularioVO.setFields(fieldsList);
                    formularios.add(formularioVO);
                }

            }
        }catch (Exception e){
            Log.e("Parsing error", e.getLocalizedMessage());
        }
        return formularios;
    }


}
