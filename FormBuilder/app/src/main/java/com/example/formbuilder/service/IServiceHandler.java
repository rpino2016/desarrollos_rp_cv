package com.example.formbuilder.service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rafaelpino on 5/3/16.
 */
public interface IServiceHandler {
    public void onWSResponse(ArrayList response, int responseType);
    public void onWSError(int errorCode);
}
