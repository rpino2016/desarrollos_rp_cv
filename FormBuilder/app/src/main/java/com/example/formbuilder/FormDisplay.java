package com.example.formbuilder;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.formbuilder.vo.FormularioVO;

import java.util.ArrayList;

/**
 * Created by rafaelpino on 5/3/16.
 */
public class FormDisplay extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formdisplay);
        ArrayList<FormularioVO> xmlForm = getIntent().getParcelableArrayListExtra(MainActivity.EXTRA_FORMS);

        if (xmlForm != null) {
            displayForm(xmlForm);
        }
    }

    /***
     * Metodo para construir el formulario dinamicamente con el xml recibido
     *
     * @param formXML List con los objetos que representan el formulario
     * @return
     */
    public boolean displayForm(ArrayList<FormularioVO> formXML) {


        try {
            ScrollView scrollView = new ScrollView(this);
            final LinearLayout layout = new LinearLayout(this);
            layout.setOrientation(android.widget.LinearLayout.VERTICAL);
            for (int i = 0; i < formXML.size(); i++) {
                FormularioVO formularioVO = formXML.get(i);
                setTitle(formularioVO.getNombre());

                //TODO Recorrer el array list y producir los input de acuerdo al campo type
                TextView label = new TextView(this);
                label.setText(formularioVO.getNombre());
                layout.addView(label);

                scrollView.addView(layout);
            }
            setContentView(scrollView);
            return true;
        } catch (Exception e) {
            Log.e("Error", "Error Displaying Form");
            e.printStackTrace();
            return false;
        }
    }
}
