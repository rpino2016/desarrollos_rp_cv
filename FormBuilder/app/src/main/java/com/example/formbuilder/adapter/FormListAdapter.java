package com.example.formbuilder.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.formbuilder.R;
import com.example.formbuilder.vo.FormularioVO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rafaelpino on 5/7/16.
 */
public class FormListAdapter extends BaseAdapter {
    List<FormularioVO> forms;
    Context context;

    public FormListAdapter(Context context, List<FormularioVO> forms){
        this.forms = forms;
        this.context = context;
    }

    @Override
    public int getCount() {
        return this.forms.size();
    }

    @Override
    public Object getItem(int position) {
        return this.forms.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;

        if (convertView == null) {
            // Create a new view into the list.
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(R.layout.activity_main, parent, false);
        }

        final LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.HORIZONTAL);


        // Set data into the view.
        TextView item = new TextView(context);
        item.setTextColor(Color.BLACK);
        FormularioVO formularioVO = this.forms.get(position);
        item.setText(formularioVO.getNombre());


        ImageView icon = new ImageView(context);
        icon.setImageResource(R.drawable.form_icon);
        icon.setClickable(false);
        icon.setFocusable(false);
        int height = 60;
        int width = 60;
        LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(width,height);
        icon.setLayoutParams(parms);

        layout.addView(icon);
        layout.addView(item);

        layout.setContentDescription(""+formularioVO.getId());
        return layout;
    }
}
