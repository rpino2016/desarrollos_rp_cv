package com.example.formbuilder;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Movie;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.example.formbuilder.adapter.FormListAdapter;
import com.example.formbuilder.service.CallRestService;
import com.example.formbuilder.service.IServiceHandler;
import com.example.formbuilder.vo.FormularioVO;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements IServiceHandler, AdapterView.OnItemClickListener {
    public static final String EXTRA_FORMS = "com.example.formBuilder.FORMULARIOS";
    CallRestService callAPI = new CallRestService();
    ProgressBar progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progress = (ProgressBar) findViewById(R.id.progressBar);
        progress.setIndeterminate(true);
        progress.setVisibility(View.VISIBLE);
        callAPI.invokeWS("http://my-dynamic-form.appspot.com/services/forms.rst",null, this, CallRestService.FORM_LIST_TYPE);

    }

    @Override
    public void onWSResponse(ArrayList response, int responseType){
        progress.setVisibility(View.GONE);
        switch (responseType){
            case CallRestService.DETAILED_LIST_TYPE:
                Intent intent = new Intent(this, FormDisplay.class);
                intent.putExtra(EXTRA_FORMS,response);
                startActivity(intent);
                break;
            case CallRestService.FORM_LIST_TYPE:
                displayFormList(response);
                break;
            default:
                break;
        }

    }

    @Override
    public void onWSError(int errorCode) {
        progress.setVisibility(View.GONE);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("No se obtuvo respuesta del servidor, intente mas tarde.")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Kills app thread
                        android.os.Process.killProcess(android.os.Process.myPid());
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        callAPI.invokeWS("http://my-dynamic-form.appspot.com/services/forms.rst?formId="+view.getContentDescription(),null, this, CallRestService.DETAILED_LIST_TYPE);
    }

    private void displayFormList(ArrayList<FormularioVO> formList){
        ListView listView = (ListView) findViewById(R.id.listView);
        FormListAdapter adapter = new FormListAdapter(getBaseContext(), formList);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
    }

}
