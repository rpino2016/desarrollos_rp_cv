package com.example.formBuilder.dao;


import java.util.ArrayList;
import java.util.List;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

import com.example.formBuilder.util.FormModel;
import com.example.formBuilder.vo.FormVO;

public class FormDAO {
	
	public void insertForm(FormVO form) {
		PersistenceManager pm = JDOHelper.getPersistenceManagerFactory("transactions-optional").getPersistenceManager();
		try {
			pm.makePersistent(form);
		} finally {
			pm.close();
		}
	}
	
	public List<FormModel> getForms() {
		PersistenceManager pm = JDOHelper.getPersistenceManagerFactory("transactions-optional").getPersistenceManager();
		List<FormVO> resultSet = new ArrayList<FormVO>();
		List<FormModel> response = new ArrayList<FormModel>();
		Query q = pm.newQuery("select from " + FormVO.class.getName());
		try {
			resultSet = (List<FormVO>) q.execute();
			resultSet.size();// BUG, esto previene error en la consulta
			for(FormVO vo : resultSet){
				FormModel model = new FormModel();
				model.setId(vo.getFormID());
				model.setNombre(vo.getNombre());
				model.setData(vo.getXml().getValue());
				response.add(model);
			}
		} finally {
			q.closeAll();
			pm.close();
			resultSet = null;
		}
		return response;
	}
	
	public List<FormModel> getFormIdList() {
		PersistenceManager pm = JDOHelper.getPersistenceManagerFactory("transactions-optional").getPersistenceManager();
		List<FormVO> resultSet = new ArrayList<FormVO>();
		List<FormModel> response = new ArrayList<FormModel>();
		Query q = pm.newQuery("select from " + FormVO.class.getName());
		try {
			resultSet = (List<FormVO>) q.execute();
			resultSet.size();// BUG, esto previene error en la consulta
			for(FormVO vo : resultSet){
				FormModel model = new FormModel();
				model.setId(vo.getFormID());
				model.setNombre(vo.getNombre());
				response.add(model);
			}
		} finally {
			q.closeAll();
			pm.close();
			resultSet = null;
		}
		return response;
	}
	
	public List<FormModel> getFormById(Long id) {
		PersistenceManager pm = JDOHelper.getPersistenceManagerFactory("transactions-optional").getPersistenceManager();
		List<FormVO> resultSet = new ArrayList<FormVO>();
		List<FormModel> response = new ArrayList<FormModel>();
		Query q = pm.newQuery("select from " + FormVO.class.getName() + " where formID == "+ id);
		
		try {
			resultSet = (List<FormVO>) q.execute();
			resultSet.size();// BUG, esto previene error en la consulta
			for(FormVO vo : resultSet){
				FormModel model = new FormModel();
				model.setId(vo.getFormID());
				model.setNombre(vo.getNombre());
				model.setData(vo.getXml().getValue());
				response.add(model);
			}
		} finally {
			q.closeAll();
			pm.close();
			resultSet = null;
		}
		return response;
	}

	public boolean deleteForm(FormVO form) {
		PersistenceManager pm = JDOHelper.getPersistenceManagerFactory("transactions-optional").getPersistenceManager();
		boolean isDeleted = false;
		try {
			pm.deletePersistent(form);
			isDeleted = true;
		} finally {
			pm.close();
		}
		return isDeleted;
	}

	public boolean deleteFormByID(String id) {
		PersistenceManager pm = JDOHelper.getPersistenceManagerFactory("transactions-optional").getPersistenceManager();
		boolean removed = false;
		Transaction tx = pm.currentTransaction();
		Query q = pm.newQuery("select from " + FormVO.class.getName() + " where formID == '" + id + "'");
		List<FormVO> resultSet = new ArrayList<FormVO>();
		try {
			tx.begin();
			resultSet = (List<FormVO>) q.execute();
			if(!resultSet.isEmpty()){
				pm.deletePersistent(resultSet.get(0));
				removed = true;
			}
			tx.commit();
		} finally {
			q.closeAll();
			pm.close();
			if(tx.isActive()){
				tx.rollback();
			}
		}
		return removed;
	}

}
