package com.example.formBuilder;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.formBuilder.dao.FormDAO;
import com.example.formBuilder.util.FormModel;
import com.example.formBuilder.vo.FormVO;
import com.google.appengine.api.datastore.Text;
import com.google.gson.Gson;
@SuppressWarnings("serial")
public class FormBuilderServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		FormDAO dao = new FormDAO();
		resp.setContentType("application/json");
		String page = "./pages/index.jsp";
		String action = req.getParameter("action");
		if(action!=null){
			if(action.equals("Save")){
				FormVO formVo = new FormVO();
				formVo.setNombre(req.getParameter("name"));
				formVo.setXml(new Text(req.getParameter("formXML")));
				dao.insertForm(formVo);
			}else if (action.equals("GetAll")){
				List<FormModel> forms = dao.getForms();
				Gson gson = new Gson();
				String responseJson = gson.toJson(forms);
				// page = "./pages/editar.jsp";
				resp.getWriter().write(responseJson);
			}
			// req.getRequestDispatcher(page).forward(req, resp);
			resp.setStatus(200);
		}
	}
	
}
