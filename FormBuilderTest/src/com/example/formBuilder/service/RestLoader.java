package com.example.formBuilder.service;

import org.restlet.Application;
import org.restlet.Restlet;
import org.restlet.routing.Router;


public class RestLoader extends Application {

	@Override
	public Restlet createInboundRoot() {
        Router router = new Router(getContext());
        router.attach("/forms.rst",FormRESTService.class);
        return router;

	}

	
}
