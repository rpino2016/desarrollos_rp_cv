package com.example.formBuilder.vo;

import java.io.Serializable;

import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Text;

@PersistenceCapable
public class FormVO  implements  Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Long formID;
	@Persistent
	private String nombre;
	@Persistent(defaultFetchGroup = "true")
	private Text xml;
	
	public Long getFormID() {
		return formID;
	}
	public void setFormID(Long formID) {
		this.formID = formID;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Text getXml() {
		return xml;
	}
	public void setXml(Text xml) {
		this.xml = xml;
	}
	
}
